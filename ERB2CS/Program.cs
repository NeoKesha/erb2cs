﻿using System.IO;
using System.Net.Http.Headers;
using System.Runtime.ConstrainedExecution;

internal class Program
{
    //Full of shitcode for academic reasons
    private static void Main(string[] args)
    {
        var ns = "PluginNS";
        if (args.Length > 1)
        {
            ns = args[1];
        }
        var className = Path.GetFileNameWithoutExtension(args[0]);
        var conv = new ERBConverter(args[0], className, ns);
        var csPath = Path.GetDirectoryName(args[0]) + "\\" + className + ".cs";
        conv.Convert(csPath);
    }
}

internal class ERBConverter
{
    internal ERBConverter(string path, string name, string ns = "PluginNS")
    {
        this.path = path;
        this.name = name;
        this.ns = ns;
    }

    internal void Convert(string output)
    {
        using (var file = new FileStream(path, FileMode.Open, FileAccess.Read))
        using (var result = new FileStream(output, FileMode.Create, FileAccess.Write))
        {
            var reader = new StreamReader(file);
            var writer = new StreamWriter(result);

            writer.WriteLine($"namespace {ns}.{name}");
            writer.WriteLine("{");
            identationLevel = 1;

            while (!reader.EndOfStream)
            {
                ParseLine(reader.ReadLine(), writer);
            }
            Finalize(writer);

            writer.WriteLine("}");
            writer.Flush();
        }
    }

    private string path;
    private string name;
    private string ns;

    private void ParseLine(string line, StreamWriter writer)
    {
        var trimmedLine = line.Trim();
        if (trimmedLine.Length == 0)
        {
            return;
        }

        if (trimmedLine.StartsWith(';'))
        {
            writer.WriteLine(GetIdentation() + trimmedLine.Replace(";", "//"));
            return;
        }

        if (trimmedLine.StartsWith('@'))
        {
            if (methods.Count > 0)
            {
                methods.Last().WriteFooter(writer);
                methods.Last().completed = true;
            }
            var argsStart = trimmedLine.IndexOf('(');
            var argsEnd = trimmedLine.IndexOf(')');
            var args = trimmedLine.Substring(argsStart + 1, argsEnd - argsStart - 1).Split(',');
            var name = trimmedLine.Substring(1, argsStart - 1).Trim();
            var method = new MethodMeta(name, args);
            methods.Add(method);
            return;
        }

        if (trimmedLine.StartsWith("#FUNCTION"))
        {
            methods.Last().returnType = "object";
            return;
        }

        if (trimmedLine.StartsWith("#DIMS") && methods.Count() > 0)
        {
            var arg = trimmedLine.Replace("#DIMS", "").Trim();
            methods.Last().AddArgType(arg, "string");
            return;
        }

        if (trimmedLine.StartsWith("#DIM") && methods.Count() > 0)
        {
            var arg = trimmedLine.Replace("#DIM", "").Trim();
            methods.Last().AddArgType(arg, "Int64");
            return;
        }

        if (!methods.Last().argsReady)
        {
            methods.Last().argsReady = true;
            methods.Last().WriteHeader(writer);
            identationLevel = 3;
        }

        if (trimmedLine.StartsWith("FOR"))
        {
            var p = trimmedLine.Replace("FOR ", "").Split(",");
            
            writer.WriteLine($"{GetIdentation()}for (var {p[0]} = {FormatArg(p[1])}, {p[0]} <= {FormatArg(p[2])}, ++{p[0]}) {"{"}");
            ++identationLevel;
            return;
        }

        if (trimmedLine.StartsWith("NEXT"))
        {
            --identationLevel;
            writer.WriteLine($"{GetIdentation()}{"}"}");
            return;
        }

        if (trimmedLine.StartsWith("SELECTCASE"))
        {
            var varName = trimmedLine.Split(" ")[1];
            writer.WriteLine($"{GetIdentation()}switch ({FormatArg(varName)}) {"{"}");
            ++identationLevel;
            needBreak = false;
            return;
        }

        if (trimmedLine.StartsWith("CASEELSE"))
        {
            if (needBreak)
            {
                writer.WriteLine($"{GetIdentation()}break;");
                --identationLevel;
            }
            writer.WriteLine($"{GetIdentation()}default:");
            ++identationLevel;
            needBreak = true;
            return;
        }

        if (trimmedLine.StartsWith("CASE"))
        {
            if (needBreak)
            {
                writer.WriteLine($"{GetIdentation()}break;");
                --identationLevel;
            }
            var valName = trimmedLine.Split(" ")[1];
            writer.WriteLine($"{GetIdentation()}case {valName}:");
            ++identationLevel;
            needBreak = true;
            return;
        }

        if (trimmedLine.StartsWith("ENDSELECT"))
        {
            writer.WriteLine($"{GetIdentation()}break;");
            --identationLevel;
            writer.WriteLine($"{GetIdentation()}{"}"}");
            needBreak = false;
            return;
        }

        if (trimmedLine.StartsWith("CALL") || trimmedLine.StartsWith("CALLF"))
        {
            var call = trimmedLine;
            if (call.StartsWith("CALLF"))
            {
                call = call.Substring(5);
            } else if (call.StartsWith("CALL"))
            {
                call = call.Substring(4);
            }
            var argsStart = call.IndexOf('(');
            var argsEnd = call.IndexOf(')');
            var name = call.Substring(1, argsStart - 1).Trim();
            writer.WriteLine($"{GetIdentation()}{name}PluginMethod.{name}{call.Substring(argsStart, argsEnd - argsStart + 1)};");
            return;
        }

        if (trimmedLine.StartsWith("RETURNF"))
        {
            writer.WriteLine($"{GetIdentation()}return null;///{trimmedLine}");
            return;
        }
        if (trimmedLine.StartsWith("RETURN"))
        {
            writer.WriteLine($"{GetIdentation()}return;");
            return;
        }

        writer.WriteLine($"{GetIdentation()}///{trimmedLine}");
    }

    private void Finalize(StreamWriter writer)
    {
        if (methods.Count == 0)
        {
            return;
        }

        if (!methods.Last().completed)
        {
            methods.Last().WriteFooter(writer);
            methods.Last().completed = true;
        }
    }

    private string GetIdentation()
    {
        string str = "";
        for (int i = 0; i < identationLevel; ++i)
        {
            str += "\t";
        }

        return str; 
    }

    List<MethodMeta> methods = new List<MethodMeta>();
    int identationLevel = 0;
    bool needBreak = false;

    internal string FormatArg(string arg)
    {
        if (arg.StartsWith("ARGS"))
        {
            if (arg.Contains(':'))
            {
                var idx = Int64.Parse(arg.Split(":")[1]);
                return $"ARGS{idx}";
            }
            else
            {
                return "ARGS";
            }
        }
        else if (arg.StartsWith("ARG"))
        {
            if (arg.Contains(':'))
            {
                var idx = Int64.Parse(arg.Split(":")[1]);
                return $"ARG{idx}";
            }
            else
            {
                return "ARG";
            }
        } else
        {
            return arg;
        }
    }
}

internal class MethodMeta
{
    internal MethodMeta(string name, string[] args)
    {
        this.name = name;
        argsReady = false;
        completed = false;
        argumets = new string[args.Length];
        for (var i = 0; i < args.Length; ++i)
        {
            var arg = args[i];
            if (arg.Contains('='))
            {
                arg = arg.Substring(0, arg.IndexOf('='));
            }
            argumets[i] = arg.Trim();
        }
    }

    public void AddArgType(string arg, string type)
    {
        argTypes.Add(arg, type);
    }

    public void WriteHeader(StreamWriter writer)
    {
        /*
         [SupportedOSPlatform("windows")]
    public class CtrlClothesSet : IPluginMethod
    {
        public string Name => "CtrlClothesSet";

        public string Description => "@CTRL_CLOTHES_SET native implementation";

        public void Execute(PluginMethodParameter[] args)
        {
         */

        writer.WriteLine("\t[SupportedOSPlatform(\"windows\")]");
        writer.WriteLine($"\tpublic class {name}PluginMethod : IPluginMethod {"{"}");
        writer.WriteLine($"\t\tpublic String Name => \"{name}\";");
        writer.WriteLine($"\t\tpublic string Description => \"@{name} native implementation\";");
        writer.WriteLine($"\t\tpublic void Execute(PluginMethodParameter[] args) {"{"}");
        var argLine = "";
        var argTypeLine = "";
        for (var i = 0; i < argumets.Length; ++i)
        {
            var arg = argumets[i];
            if (arg.StartsWith("ARGS"))
            {
                if (arg.Contains(':'))
                {
                    var idx = Int64.Parse(arg.Split(":")[1]);
                    writer.WriteLine($"\t\t\tstring ARGS{idx} = args[{i}].strValue;");
                    argTypeLine += $"string ARGS{idx}";
                    argLine += $"ARGS{idx}";
                } else
                {
                    writer.WriteLine($"\t\t\tstring ARGS = args[{i}].strValue;");
                    argTypeLine += $"string ARGS";
                    argLine += "ARGS";
                }
            }
            else if (arg.StartsWith("ARG"))
            {
                if (arg.Contains(':'))
                {
                    var idx = Int64.Parse(arg.Split(":")[1]);
                    writer.WriteLine($"\t\t\tInt64 ARG{idx} = args[{i}].intValue;");
                    argTypeLine += $"Int64 ARG{idx}";
                    argLine += $"ARG{idx}";
                }
                else
                {
                    writer.WriteLine($"\t\t\tInt64 ARG = args[{i}].intValue;");
                    argTypeLine += $"Int64 ARG";
                    argLine += "ARG";
                }
            }
            else if(argTypes[arg] == "string")
            {
                writer.WriteLine($"\t\t\tstring {arg} = args[{i}].strValue;");
                argTypeLine += $"string {arg}";
                argLine += arg;
            } else
            {
                writer.WriteLine($"\t\t\tInt64 {arg} = args[{i}].intValue;");
                argTypeLine += $"Int64 {arg}";
                argLine += arg;
            }
            
            if (i < argumets.Length - 1)
            {
                argLine += ", ";
                argTypeLine += ", ";
            }
        }
        writer.WriteLine();
        writer.WriteLine($"\t\t\t{name}PluginMethod.{name}({argLine});");
        writer.WriteLine("\t\t}");
        writer.WriteLine("");
        writer.WriteLine($"\t\tinternal static {returnType} {name}({argTypeLine}) {"{"}");
    }

    public void WriteFooter(StreamWriter writer)
    {
        writer.WriteLine("\t\t}");
        writer.WriteLine("\t}");
    }

    public bool argsReady;
    public bool completed;
    public string returnType = "void";
    public string name;
    public string[] argumets;
    public Dictionary<string, string> argTypes = new Dictionary<string, string>();
}